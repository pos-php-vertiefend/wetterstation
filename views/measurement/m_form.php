<div class="row">
    <div class="col-md-4">
        <div class="form-group required <?= $model->hasError('time') ? 'has-error' : ''; ?>">
            <label class="control-label">Zeitpunkt *</label>
            <input type="text" class="form-control" name="time" value="<?= $model->getTime() ?>">

            <?php if ($model->hasError('time')): ?>
                <div class="help-block"><?= $model->getError('name') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <div class="form-group required <?= $model->hasError('temp') ? 'has-error' : ''; ?>">
            <label class="control-label">Temperatur[°C] *</label>
            <input type="text" class="form-control" name="temp" value="<?= $model->getTemperature() ?>">

            <?php if ($model->hasError('temp')): ?>
                <div class="help-block"><?= $model->getError('temp') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <div class="form-group required <?= $model->hasError('rain') ? 'has-error' : ''; ?>">
            <label class="control-label">Regenmenge[ml] *</label>
            <input type="text" class="form-control" name="rain" value="<?= $model->getRain() ?>">

            <?php if ($model->hasError('rain')): ?>
                <div class="help-block"><?= $model->getError('altitude') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <div class="form-group required <?= $model->hasError('station') ? 'has-error' : ''; ?>">
            <label class="control-label">Station *</label>
            <input type="text" class="form-control" name="station" value="<?= $model->getStation()->getName() ?>">

            <?php if ($model->hasError('station')): ?>
                <div class="help-block"><?= $model->getError('station') ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>

