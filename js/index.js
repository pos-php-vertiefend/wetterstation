$(document).ready(function () {
    $("#btnSearch").click(function () {
        showChart();
        loadAllMeasurements();
    });
});

function parseMeasurementsTable(data) {
    var tmp = "";
    tmp += "<table class=\"table table-striped table-bordered\">"
    tmp += "<thead>\n" +
        "            <tr>\n" +
        "                <th>Zeitpunkt</th>\n" +
        "                <th>Temperatur [C°]</th>\n" +
        "                <th>Regenmenge [ml]</th>\n" +
        "                <th></th>\n" +
        "            </tr>\n" +
        "            </thead>"

    $.each(data, function (index, measurements) {
        tmp += "<tr>";
        tmp += "<td>" + measurements.time + "</td>";
        tmp += "<td>" + measurements.temperature + "</td>";
        tmp += "<td>" + measurements.rain + "</td>";
        tmp += "<td>";
        tmp += '<a class="btn btn-info" href="index.php?r=measurement/view&id=' + measurements.id + '"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;'
        tmp += '<a class="btn btn-primary" href="index.php?r=measurement/update&id=' + measurements.id + '"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;'
        tmp += '<a class="btn btn-danger" href="index.php?r=measurement/delete&id=' + measurements.id + '"><span class="glyphicon glyphicon-remove"></span></a>';
        tmp += "</td>";
        tmp += "</tr>";
        tmp += "</table>"
        //tmp += "</tbody>"
    });

    $("#measurements").html(tmp);
    //return tmp;

}

function parseMeasurementsChart(data) {
    var tmp = "";


    $("#chart").html(tmp);
    //return tmp;

}

function loadAllMeasurements() {

    $.get("api.php?r=/station/"+$("#station_id option:selected").val()+"/measurement", function (data) {
        $("#credentials").html(parseMeasurementsTable(data));

    });
}

function showChart(){
    $.get("api.php?r=/station/"+$("#station_id option:selected").val()+"/measurement", function (data) {
        $("#credentials").html(parseMeasurementsChart(data));

    });
}


